
%{
	#include<stdio.h>
	#include "y.tab.h"
	 
%}

%%
"120" {return CheckTOK;}

"06" return IMEITOK;
"0*" return HelpTOK;
"67" {return Call_fwdTOK;}
"128" {return Send_credTOK;}
"126" {return PLS_callTOK;}
"*" return starTOK;
[1][8][7][6][0-9]{7} {yylval.string=strdup(yytext); return p_numTOK;}
[1][5-9]|[2-9][0-9]+|[1-9][0-9][0-9]+ {yylval.num=atoi(yytext); return AmtTOK;}
[0-9]  return digittok;
[a-zA-Z] {return letterTOK;}
[!,@$%^&()-_=+.?:;'"] {return symbolTOK;}
"#" {return hashTOK;}
\n /**/


%%
 
 