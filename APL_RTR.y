%{
       #include <stdio.h>
       #include <stdlib.h>
       #include <time.h>
       

       
	//Handles errors made with MMI Code input
	void yyerror (char const *s) 
	{
		fprintf (stderr, "%s\n", s);
	}
	int credit_bal;
	int amt,x,key,state=0,state2=0;
	char* phone_num,imei_gen[15],*IMEI;
	char digit_list[]="0123456789";

	


       int yywrap()
       {
               return 1;
       }

       void Send_credit(char *pnum ,int amt)
       {
       // checks if credit can be sent or the balance is too low takes into consideration the $5 for sending credit

		if (amt +5 <= credit_bal)
				{
					
					printf("sending credit to '%s'\n",pnum);
					credit_bal-=(amt +5);
				}
				else
				{
					printf("insufficient balance \n");
				}
       }

       char * Produce_IMEI()
       {
       //function used to generate an IMEI buy using random numbers to populate a string
		for (x=0; x<15; x++)
			{
				key=rand()%9;
				imei_gen[x]= digit_list[key];
			}
			return imei_gen;
       }
       int setcredit()
       {
		int set;
		set = rand()%1001;
		return set;
       }

      int main()
       {
	char choice;
	int x;
	srand(time(0));
		//Main display options
		printf("***************************************************\n");
		printf("**********************WELCOME**********************\n");
		printf("***************************************************\n");
		printf("*Features                              |MMI Code\n");
		printf("*______________________________________________\n");
		printf("*Display the IMEI number of the phone  |*#06#\n");
		printf("*General text help mode                |*#0*#\n");
		printf("*Check if call-forwarding is active    |*#67#\n");
		printf("*Send credit to another cell phone     |*128*1876number*amount#\n");
		printf("*Send \"Please call me\"                 |*126*1876number#\n");
		printf("*Check credit balance                  |*120#\n");
		printf("****************************************************");
		printf("\nPlease enter the MMI Code for the feature you would like below in the dial pad");
		printf("\nDial Pad (): \t");
		//This function reads tokens, executes actions, and ultimately returns when it encounters end-of-input or an unrecoverable syntax error.
		x=yyparse();
		
		//Checks yyparse response 
		/*if (x==1)
		{
		 printf("Good bye\n");
		 system("pause");
		 exit(0);
		}*/

	      system("pause");
	      printf("Do you want to enter another code? (y or n) \n");
	      scanf("%c",&choice);

	      if (choice== 'y')
	      {
		system("cls");
		main();
		
	      }
	      else
	      {
		printf("Good bye \n");
		system("pause");
		
	      }
	      
       }

       %}

       %token IMEITOK hashTOK PLS_callTOK Send_credTOK Call_fwdTOK CheckTOK starTOK HelpTOK digittok letterTOK symbolTOK
	%union // defines a union so that yylval can store a string and an int
	{
		int num;
		char *string;
	}
	%token <num> AmtTOK
	%token <string> p_numTOK

	%%
       commands:starTOK command hashTOK 
		;

	command:Code1
		|
		Code2
		|
		Code3
		;

		Code1: hashTOK A
			;

		A: IMEITOK hashTOK
			{
				if(state==0)
				{
				//used to maintain the value of the imei while the program is still running
					IMEI=Produce_IMEI();
					state=1;
				}
				printf("\a");
				printf("Your IMEI is %s\n",IMEI);
				YYACCEPT;
			}
		   |
		   Call_fwdTOK hashTOK
			{
			printf("Call forwarding is on/off\n");
			YYACCEPT;
			}
		   |
		    HelpTOK hashTOK 
			{
			printf("\a");
			printf("help reached\n");
			YYACCEPT;
			}
		    ;

		 Code2: PLS_callTOK starTOK B hashTOK
			{
			printf("\a");
			printf("Please call me sent to %s\n",phone_num);
			YYACCEPT;
			}
			|
			Send_credTOK starTOK B D hashTOK
			{
				Send_credit(phone_num,amt);
				YYACCEPT;
			}
			;

		B:  p_numTOK
			{
				phone_num=$1;
			}
		   ;
		
		D: starTOK AmtTOK
			{
				amt=$2;
			}
		   ;

		Code3: CheckTOK hashTOK
			{
			printf("\a");
			if (state2 ==0)
			{credit_bal= setcredit();
			state2=1;}
			printf("Your credit balance is %d \n",credit_bal);
			YYACCEPT;
			}
			;
	%%